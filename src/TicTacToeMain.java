import javax.swing.*;
import java.awt.*;

public class TicTacToeMain {
    public static void main(String[] args){
        TicTacToeClient ticTacToeClient = new TicTacToeClient();
        ticTacToeClient.init();
        ticTacToeClient.setSize(new Dimension(300,300));
        ticTacToeClient.setVisible(true);
    }
}
