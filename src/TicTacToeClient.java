import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.io.*;

    // Java extension packages
    import javax.swing.*;

    // Client class to let a user play Tic-Tac-Toe with
    // another user across a network.
    public class TicTacToeClient extends JApplet implements Runnable {
        private JTextField idField;
        private JTextArea displayArea;
        private JPanel boardPanel, panel2;
        private Square board[][], currentSquare;
        private Socket connection;
        private DataInputStream input;
        private DataOutputStream output;
        private Thread outputThread;
        private char myMark;
        private boolean myTurn;

    // Set up user-interface and board
    public void init() {
        Container container = getContentPane();
        // set up JTextArea to display messages to user
        displayArea = new JTextArea(4, 30 );
        displayArea.setEditable( false );
        container.add( new JScrollPane( displayArea ), BorderLayout.SOUTH );
        // set up panel for squares in board
        boardPanel = new JPanel();
        boardPanel.setLayout( new GridLayout(3, 3, 0, 0 ) );

        // create board
        board = new Square[3][3];
        // When creating a Square, the location argument to the
        // constructor is a value from 0 to 8 indicating the// position of the Square on the board. Values 0, 1,// and 2 are the first row, values 3, 4, and 5 are the// second row. Values 6, 7, and 8 are the third row.
        for ( int row = 0; row < board.length; row++ ) {
            for ( int column = 0; column < board[ row ].length; column++ ) {// create Square
                board[ row ][ column ] = new Square( ' ', row * 3 + column );
                boardPanel.add( board[ row ][ column ] );
            }
        }

        // textfield to display player's mark
        idField = new JTextField();
        idField.setEditable( false );
        container.add( idField, BorderLayout.NORTH );

        // set up panel to contain boardPanel (for layout purposes)
        panel2 = new JPanel();
        panel2.add( boardPanel, BorderLayout.CENTER );
        container.add( panel2, BorderLayout.CENTER );
        }

        // Make connection to server and get associated streams.
        // Start separate thread to allow this applet to
        // continually update its output in text area display.
        public void start() {
        // connect to server, get streams and start outputThread
        try {// make connection
            connection = new Socket(
            InetAddress.getByName( "127.0.0.1" ), 5000 );
        // get streams
        input = new DataInputStream(connection.getInputStream() );
        output = new DataOutputStream(connection.getOutputStream() );
        } catch ( IOException ioException ) {
        ioException.printStackTrace();
        }

         // create and start output thread
        outputThread = new Thread( this );
        outputThread.start();
        }

        public void run() {
            try {
                myMark = input.readChar();
                idField.setText( "You are player\"" + myMark + "\"" );
                myTurn = ( myMark == 'X' ? true : false );
            } catch ( IOException ioException ) {
                ioException.printStackTrace();
            }
            while ( true ) {
                try {
                    String message = input.readUTF();
                    processMessage(message);
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        } // end method run

        public void processMessage( String message ) {
            if ( message.equals( "Valid move." ) ) {
                displayArea.append( "Valid move, please wait.\n" );
                SwingUtilities.invokeLater(
                        new Runnable() {
                            public void run() {
                                currentSquare.setMark( myMark );
                            }
                        }); // end call to invokeLater
            } else if ( message.equals( "Invalid move, try again" ) ) {
                displayArea.append( message + "\n" );
                myTurn = true;
            } else if ( message.equals( "Opponent moved" ) ) {
                try {
                    final int location = input.readInt();
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            int row = location /3;
                            int column = location % 3;
                            board[ row ][ column ].setMark(( myMark == 'X' ? 'O' : 'X' ) );
                            displayArea.append("Opponent moved. Your turn.\n" );
                        }
                    });
                    myTurn = true;
                } catch ( IOException ioException ) {
                    ioException.printStackTrace();
                }
            } else
                displayArea.append( message + "\n" );
            displayArea.setCaretPosition(displayArea.getText().length() );
        }
        public void sendClickedSquare( int location ) {
            if ( myTurn ) {
                try {
                    output.writeInt( location );
                    myTurn = false;
                } catch ( IOException ioException ) {
                    ioException.printStackTrace();
                }
            }
        }
        public void setCurrentSquare( Square square ) {
            currentSquare = square;
        }

        private class Square extends JPanel {
            private char mark;
            private int location;


            public Square( char squareMark, int squareLocation) {
                mark = squareMark;
                location = squareLocation;
                addMouseListener(new MouseAdapter() {
                    public void mouseReleased( MouseEvent e ) {
                        setCurrentSquare( Square.this );
                        sendClickedSquare( getSquareLocation() );
                    }
                });
            } // end Square constructor

            public Dimension getPreferredSize() {
                    return new Dimension( 30, 30 );
            }
            public Dimension getMinimumSize() {
                    return getPreferredSize();
            }

            public void setMark( char newMark ) {
                    mark = newMark;
                    repaint();
            }


            public int getSquareLocation() {
                    return location;
            }


            public void paintComponent( Graphics g ) {
                super.paintComponent(g);
                g.drawRect(0, 0, 29, 29);
                g.drawString(String.valueOf(mark), 11, 20);
            }
        }
    } // end class TicTacToeClient
