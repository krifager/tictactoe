import javax.swing.*;
import java.awt.*;
import java.net.ServerSocket;
import java.io.*;
import java.net.*;

public class TicTacToeServer extends JFrame{
    private byte[] board;
    private JTextArea outputArea;
    private ServerSocket server;
    private int currentPlayer;
    private Player players[];

    public TicTacToeServer(){
        super("Tic Tac Toe server");
        board = new byte[9];
        currentPlayer = 0;

        //Open a socket
        try {
            server = new ServerSocket( 5000, 2 );
        } catch( IOException ioException ) {
            ioException.printStackTrace();
            System.exit(1);
        }


        outputArea = new JTextArea();
        getContentPane().add( outputArea, BorderLayout.CENTER );
        outputArea.setText( "Server awaiting connections\n" );
        setSize( 300, 300 );
        setVisible( true );
    }

    public void execute() {
        // wait for each client to connect
        for (int i = 0; i < players.length; i++) {
            // wait for connection, create Player, start thread
            try {
                players[i] = new Player(server.accept(), i);
                players[i].start();
            }
            // process problems receiving connection from client
            catch (IOException ioException) {
                ioException.printStackTrace();
                System.exit(1);
            }
        }
        synchronized (players[0]) {
            players[0].setSuspended(false);
            players[0].notify();
        }
    }
    public void display(String message) {
        outputArea.append( message + "\n" );
    }
    public synchronized boolean validMove(int location, int player) {
        boolean moveDone = false;

        // while not current player, must wait for turn
        while (player != currentPlayer) {
            // wait for turn
            try {
                wait();
            }
            // catch wait interruptions
            catch (InterruptedException interruptedException) {
                interruptedException.printStackTrace();
            }
        }
        // if location not occupied, make move
        if ( !isOccupied( location ) ) {
            // set move in board array
            board[location] = (byte) (currentPlayer == 0 ? 'X' : 'O' );
            // change current player
            currentPlayer = ( currentPlayer + 1 ) % 2;
            // let new current player know that move occurred
            players[ currentPlayer ].otherPlayerMoved( location );
            // tell waiting player to continue
            notify();
            // tell player that made move that the move was valid
            return true;
        }// tell player that made move that the move was not valid
        else return false;
    }
    public boolean isOccupied( int location ) {
        if ( board[ location ] == 'X' || board [ location ] == 'O')
            return true;
        else return false;
    }
    public boolean gameOver() {
        return false;
    }
    public static void main( String args[] ) {
        TicTacToeServer application = new TicTacToeServer();

        application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE );
        application.execute();
    }
    private class Player extends Thread {
        private Socket connection;
        private DataInputStream input;
        private DataOutputStream output;
        private int playerNumber;
        private char mark;
        protected boolean suspended = true;

        // set up Player thread
        public Player( Socket socket, int number ) {
            playerNumber = number;

            // specify player's mark
            mark = ( playerNumber == 0 ? 'X' : 'O' );
            connection = socket;

            // obtain streams from Socket
            try {
                input = new DataInputStream(connection.getInputStream());
                output = new DataOutputStream(connection.getOutputStream());
            } catch(IOException ioException) {
                ioException.printStackTrace();
                System.exit(1);
            }
        }
        // send message that other player moved; message contains
        // a String followed by an int
        public void otherPlayerMoved( int location ) {
            // send message indicating move
            try {
                output.writeUTF( "Opponent moved" );
                output.writeInt( location );
            }
            // process problems sending message
            catch ( IOException ioException ) {
                ioException.printStackTrace();
            }
        }
        // control thread's execution
        public void run() {
            // send client message indicating its mark (X or O),
            // process messages from client
            try {
                display( "Player " + ( playerNumber == 0 ? 'X' : 'O' ) + " connected" );// send player's mark
                output.writeChar( mark );
                // send message indicating connection
                output.writeUTF( "Player " + ( playerNumber == 0 ? "X connected\n" : "O connected, please wait\n" ) );
                // if player X, wait for another player to arrive
                if ( mark == 'X' ) {
                    output.writeUTF( "Waiting for another player" );
                    // wait for player O
                    try {
                        synchronized( this ) {
                            while ( suspended )
                                wait();
                        }
                    } catch ( InterruptedException exception ) {
                        exception.printStackTrace();
                    }
                    output.writeUTF("Other player connected. Your move." );
                }
                // while game not over
                while ( ! gameOver() ) {// get move location from client
                    int location = input.readInt();

                    // check for valid move
                    if ( validMove( location, playerNumber ) ) {
                        display( "loc: " + location );
                        output.writeUTF( "Valid move." );
                    } else
                        output.writeUTF( "Invalid move, try again" );
                }

                connection.close();
            } catch( IOException ioException ) {
                ioException.printStackTrace();
                System.exit(1);
            }
        }
        // set whether or not thread is suspended
        public void setSuspended( boolean status ) {
            suspended = status;
        }
    } // end class Player
}
